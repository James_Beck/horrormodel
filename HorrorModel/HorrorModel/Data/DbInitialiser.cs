﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HorrorModel.Models;

namespace HorrorModel.Data
{
    public class DbInitialiser
    {
        public static void Initialise(HorrorContext context)
        {
            context.Database.EnsureCreated();

            if (context.Horrors.Any())
            {
                return;
            }

            var horrors = new Horror[]
            {
                new Horror{First="Little Emily vanished last year.", Second="Now they’re pouring new sidewalks in my neighborhood,", Third="and I’ve found her name in the wet cement,", Fourth="written in remembrance.", Fifth=" But it was written in reverse.And from below."},
                new Horror{First="I begin tucking him into bed and he tells me,", Second="“Daddy check for monsters under my bed.”", Third="I look underneath for his amusement and see him, another him, under the bed,", Fourth="staring back at me quivering and whispering,", Fifth="“Daddy there’s somebody on my bed.”"},
                new Horror{First="Don’t be scared of the monsters,", Second="just look for them.", Third="Look to your left, to your right,", Fourth="under your bed, behind your dresser,", Fifth="in your closet, but never look up, she hates being seen."},
                new Horror{First="They delivered the mannequins", Second="in bubble wrap.", Third="From the main room ", Fourth="I begin to hear", Fifth="popping."},
                new Horror{First="You hear your mom calling you into the kitchen.", Second="As you are heading down the stairs", Third="you hear a whisper", Fourth="from the closet saying,", Fifth="“Don’t go down there honey, I heard it too.”"},
            };             

            foreach(Horror h in horrors)
            {
                context.Horrors.Add(h);
            }

            context.SaveChanges();
        }
    }
}
