﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HorrorModel.Models;
using Microsoft.EntityFrameworkCore;

namespace HorrorModel.Data
{
    public class HorrorContext : DbContext
    {
        public HorrorContext(DbContextOptions<HorrorContext> options) : base(options)
        { }
        
        public DbSet<Horror> Horrors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Horror>().ToTable("Course");
        }
    }
}
