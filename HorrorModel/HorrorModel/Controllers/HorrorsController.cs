﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HorrorModel.Data;
using HorrorModel.Models;

namespace HorrorModel.Controllers
{
    public class HorrorsController : Controller
    {
        private readonly HorrorContext _context;

        public HorrorsController(HorrorContext context)
        {
            _context = context;
        }

        // GET: Horrors
        public async Task<IActionResult> Index()
        {
            return View(await _context.Horrors.ToListAsync());
        }

        // GET: Horrors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horror = await _context.Horrors
                .SingleOrDefaultAsync(m => m.ID == id);
            if (horror == null)
            {
                return NotFound();
            }

            return View(horror);
        }

        // GET: Horrors/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Horrors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,First,Second,Third,Fourth,Fifth")] Horror horror)
        {
            if (ModelState.IsValid)
            {
                _context.Add(horror);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(horror);
        }

        // GET: Horrors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horror = await _context.Horrors.SingleOrDefaultAsync(m => m.ID == id);
            if (horror == null)
            {
                return NotFound();
            }
            return View(horror);
        }

        // POST: Horrors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,First,Second,Third,Fourth,Fifth")] Horror horror)
        {
            if (id != horror.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horror);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorrorExists(horror.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(horror);
        }

        // GET: Horrors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horror = await _context.Horrors
                .SingleOrDefaultAsync(m => m.ID == id);
            if (horror == null)
            {
                return NotFound();
            }

            return View(horror);
        }

        // POST: Horrors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horror = await _context.Horrors.SingleOrDefaultAsync(m => m.ID == id);
            _context.Horrors.Remove(horror);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorrorExists(int id)
        {
            return _context.Horrors.Any(e => e.ID == id);
        }
    }
}
